import { useState } from 'react';

export default function useUser() {
  const getUser = () => {
    return sessionStorage.getItem('user');
  }

  const [user, setUser] = useState(getUser());

  const saveUser = userToken => {
    sessionStorage.setItem('user', userToken);
    setUser(userToken);
  }

  const deleteUser = () => {
    sessionStorage.setItem('user', null);
    setUser(null);
  }

  
  return {
    setUser: saveUser,
    unsetUser: deleteUser,
    user: user
  }
}