import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import RoutesDefinition from './components/RoutesDefinition'
import Navbar from "./components/Menu/Navbar"
import Auth from "./components/Authentication/Auth"
import Footer from "./components/Footer"

import useToken from "./hooks/tokenHook"

function App() {
  const {token, setToken, unsetToken} = useToken()
  console.log(`[App]: Token is ${token}`)

  return (<>
    {/* Auth or MENU */}
    {token  ?
      <Router>
        <Navbar unsetToken={unsetToken}/>
        <RoutesDefinition setToken={setToken}/>
        {/* la router def cred ca ar trebui sa tin cont de user si doar asa sa definesc anumite rute */}
      </Router>
    :
      <Auth setToken={setToken}/>
    }
    <Footer />
    
  </>)
}

export default App;
