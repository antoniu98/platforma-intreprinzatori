import React, { useEffect } from 'react';
import axios from "axios"


export default function Footer() { 
    let PLN = "?"
    let GBP = "?"
    let USD = "?"
    let BTC = '?'
    let C = '?'

    useEffect( async() => await requestAll(), [])

    const requestAll = async () => {
        await requestBitcoin()
        await refreshRates()
        await requestWeather()
        console.log(`[FOOTER] Requested`)
    }

    const requestWeather = async () => {
        const url = 'http://api.weatherapi.com/v1/current.json?key=76dd2375e4b5408fb60225023212605&q=Bucharest&aqi=no'

        await axios.get(url)
        .then(response => {
            C = response.data.current.temp_c
            document.getElementById("temp-buc").innerHTML = `Temperatura in bucuresti: ${C} grade celsius`
        })
        .catch(err => {
            console.log(`[weather] Error ${err}`)
        })
    }

    const requestBitcoin = async () => {
        const url = 'https://apiv2.bitcoinaverage.com/convert/global?from=BTC&to=USD&amount=1'

        await axios.get(url, {headers: {'x-ba-key': "OWVhMDQwYTlmMjM1NDRkOWE2OGZkNGZhYTJkNzhjNTY"}})
        .then(response => {
            
            console.log(`[rates] Response ${JSON.stringify(response.data)}`)
            BTC = response.data.price
            document.getElementById("pret-bitcoin").innerHTML = `Pret bitcoin: ${BTC} USD`
        })
        .catch(err => {
            console.log(`[btc] Error ${err}`)
        })
    }
    
    const refreshRates = async () => {
        const url = "http://api.exchangeratesapi.io/v1/latest?access_key=2a402981ac438abc99d625230bfecf43&symbols=USD,GBP,PLN"

        await axios.get(url, {})
        .then(response => {
            // console.log(`[rates] Response ${JSON.stringify(response.data.rates)}`)
            ({USD, GBP, PLN} = response.data.rates)
            document.getElementById("curs-valutar").innerHTML = `Curs EURO: 1 EUR este: ${USD} USD, ${GBP} GBP, ${PLN} PLN`
        })
        .catch(err => {
            console.log(`[rates] Error ${err}`)
        })
    }
    
    return(<>
    <footer class="footer" padding='8px' background-color='red' text-color='white' >
        <a id="pret-bitcoin">Pret bitcoin: {BTC} USD</a><br></br>
        <a id="temp-buc">Temperatura in bucuresti: {C} grade celsius</a><br></br>
        <a id="curs-valutar">Curs EURO: 1 EUR este: {USD} USD, {GBP} GBP, {PLN} PLN</a>
        <button onClick={() => requestAll()}>⟲</button>
    </footer>
    </>)
}