import React, { Component, useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import RoutesDefinition from "../RoutesDefinition"
import Signin from "./Signin"
import SignUp from "./Signup"

export default function Auth({setToken}) {
    return (
        <Router>
            <div className="App">

            <nav className="navbar navbar-expand-lg navbar-light fixed-top">
                <div className="container">
                <Link className="navbar-brand" to={"/sign-in"}>Antrep</Link>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link className="nav-link" to={"/sign-in"}>Sign in</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
                    </li>
                    </ul>
                </div>
                </div>
            </nav>

            <div className="outer">
                <div className="inner">
                    <Switch>
                        <Route exact path='/'>
                            <Signin setToken={setToken} />
                        </Route>
                        
                        <Route path="/sign-in">
                            <Signin setToken={setToken} />
                        </Route>
                        
                        <Route path="/sign-up">
                            <SignUp />
                        </Route>
                        
                    </Switch>
                </div>
            </div>

            </div>
        </Router>
    )
}