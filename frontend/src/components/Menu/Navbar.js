import React from 'react';
import { Link } from "react-router-dom"
import {Navbar as NavBar, Nav, Form, Button, FormControl, NavLink } from "react-bootstrap"
import { useHistory } from "react-router-dom"

export default function Navbar({unsetToken}) {
    const userRole = "ADMIN" // TODO
    const history = useHistory()

    function signout() {
        unsetToken()
        history.push("/sign-in")
    }

    /* ============= NAVBAR ELEMENTS: ================= */
    const CLIENT_NAVBAR = () => {
        return (<>
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/ads">Anunturi</Nav.Link>
                    <Nav.Link as={Link} to="/profile">Profil</Nav.Link>
                </Nav>
                {/* <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-info">Search</Button>
                </Form> */}
        </>)
    }

    const ENTERPRISER_NAVBAR = () => {
        return (<>
            
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/ads">Anunturi</Nav.Link>
                    <Nav.Link as={Link} to="/my-ads">Anunturile mele</Nav.Link>
                    <Nav.Link as={Link} to="/add-ad">Adauga anunt</Nav.Link>
                    <Nav.Link as={Link} to="/profile">Profil</Nav.Link>
                </Nav>
        </>)
    }

    const ADMIN_NAVBAR = () => {
        return (<>
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/ads">Anunturi</Nav.Link>
                    <Nav.Link as={Link} to="/users">Utilizatori</Nav.Link>
                    <Nav.Link as={Link} to="/profile">Profil</Nav.Link>
                    <Nav.Link as={Link} to="/statistics">Statistici</Nav.Link>
                </Nav>
        </>)
    }

    

    return ( 
    <>
        <NavBar bg="dark" variant="dark">
            <NavBar.Brand>Antrep</NavBar.Brand>
            {userRole == "CLIENT"       && CLIENT_NAVBAR() }
            {userRole == "ENTERPRISER"  && ENTERPRISER_NAVBAR() }
            {userRole == "ADMIN"        && ADMIN_NAVBAR() }
            <Button variant="outline-info" onClick={signout}>Logout</Button>
        </NavBar>
    </>
    )
}