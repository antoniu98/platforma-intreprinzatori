import React, {useState} from 'react';
import useUser from "../../hooks/userHook"
import useToken from "../../hooks/tokenHook"

import axios from "axios"

import { Form, Col, Row, Button} from 'react-bootstrap';

export default function AddAd() {
    const {user} = useUser()
    const {token, setToken, unsetToken} = useToken()

    const [adName, setAdName] = useState()
    const [description, setDescription] = useState()
    const [err, setErr] = useState()

    const [message, setMessage] = useState()

    const addAd = async (event) => {
        event.preventDefault()
        const data = { 
            token: token,
            enterpriseName: JSON.parse(user).fullName,
            email: JSON.parse(user).email,
            adName: adName,
            description: description
        }

        await axios.post("http://localhost:8888/api/ads", data)
        .then(response => {
            setMessage( response.data)
            setErr(null)
            console.log(`[addAd]: Message ${message}`)
        })
        .catch(err => {
            setErr(err)
            setMessage(null)
            console.log(`[addAd]: Eroare ${err}`)
        })
    }

    return (<>
        <h1>Adauga un anunt:</h1>
        <Form onSubmit={addAd}>
            <Form.Group as={Row}>
                <Form.Label column sm="2">
                Nume anunt
                </Form.Label>
                <Col sm="10">
                <Form.Control type="text" plaintext placeholder="Nume anunt" onChange={e => setAdName(e.target.value)}/>
                </Col>
            </Form.Group>

            <Form.Group as={Row}>
                <Form.Label column sm="2">
                Descriere anunt
                </Form.Label>
                <Col sm="10">
                <Form.Control type="text" plaintext placeholder="Descriere anunt" onChange={e => setDescription(e.target.value)}/>
                </Col>
            </Form.Group>

            <Form.Group as={Row} >
                <Form.Label column sm="2">
                Postat de:
                </Form.Label>
                <Col sm="10">
                <Form.Control plaintext readOnly defaultValue={JSON.parse(user).fullName} />
                </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="formPlaintextEmail">
                <Form.Label column sm="2">
                Email
                </Form.Label>
                <Col sm="10">
                <Form.Control plaintext readOnly defaultValue={JSON.parse(user).email} />
                </Col>
            </Form.Group>

            <Button className="btn btn-primary btn-large centerButton" type="submit">Adauga anuntul</Button>
        </Form>

        {message && <h3>Adaugare finalizata!</h3>}
        {err && <a><br></br>Completati toate campurile!</a>}
    </>)
}