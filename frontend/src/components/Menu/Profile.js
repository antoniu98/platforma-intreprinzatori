import React, {useState} from 'react';
import useUser from "../../hooks/userHook"
import useToken from "../../hooks/tokenHook"

import axios from "axios"

import { Form, Col, Row, Button} from 'react-bootstrap';

export default function Profile() {
    const { user, setToken, unsetToken } = useUser()
    const [message, setMessage] = useState()

    const editProfile = (event) => {
        event.preventDefault()
        setMessage(true)
    }

    return (<>
        <h1>Profile</h1>

        <Form onSubmit={editProfile}>
            <Form.Group as={Row}>
                <Form.Label column sm="2">
                Nume intreg:
                </Form.Label>
                <Col sm="10" background="red">
                <Form.Control type="text" plaintext defaultValue={JSON.parse(user).fullName}/>
                </Col>
            </Form.Group>

            <Form.Group as={Row} >
                <Form.Label column sm="2">
                Rol:
                </Form.Label>
                <Col sm="10">
                <Form.Control plaintext readOnly defaultValue={JSON.parse(user).role} />
                </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="formPlaintextEmail">
                <Form.Label column sm="2">
                Email
                </Form.Label>
                <Col sm="10">
                <Form.Control plaintext defaultValue={JSON.parse(user).email} />
                </Col>
            </Form.Group>

            <Button className="btn btn-primary btn-large centerButton" type="submit">Modifica profilul</Button>

            {message && <a><br></br>Profilul a fost modificat!</a>}
        </Form>
    </>)
}