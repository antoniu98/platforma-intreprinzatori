import React, { useState, useEffect } from 'react';
import { ListGroup, Card, Button, Dropdown } from 'react-bootstrap'
import { Form, Col, Row} from 'react-bootstrap';
import { Link } from "react-router-dom"
import axios from "axios"

import useToken from '../../hooks/tokenHook'
import useUser from '../../hooks/userHook'

async function getUsers(token, setUsers, setErr) {
    await axios.get("http://localhost:8888/api/users", { headers: {token: token}})
    .then(response => {
        setUsers( response.data?.users)
    })
    .catch(err => {
        setErr(err)
    })
}

export default function Users() {
    const [userElements, setUsers] = useState([])
    const [whatToContain, setWhat] = useState('')
    const [err, setErr] = useState()

    const {token} = useToken()
    const {user} = useUser()

    const filterByUserName = (u) => u.fullName.includes(whatToContain)

    useEffect( async () => {
        await getUsers(token, setUsers, setErr)
    }, []) // did mount

    const filterUsers = (event) => {
        event.preventDefault()
        setWhat(event.target.value)
    }


    /* ADMIN  */
    const editUser = async (user) => {
        const data = {
            token: token,
            fullName: user.fullName,
            email: user.email,
            role: user.role
        }
        await axios.put(`http://localhost:8888/api/users/${user._id}`, data)
        .then(response => {
            user.success = true
            setErr(false)
        })
        .catch(err => {
            user.success = false
            setErr(true)
            console.log(`[editAd]: Eroare ${err}`)
        })
    }

    const deleteUser = async (user) => {
        await axios.delete(`http://localhost:8888/api/users/${user._id}`, { headers: {token:token}})
        .then(response => {
            user.success = "deleted"
            setErr(false)
        })
        .catch(err => {
            user.success = "not_deleted"
            setErr(true)
            console.log(`[deleteAd]: Eroare ${err}`)
        })
    }

    if (JSON.parse(user).role != "ADMIN") return <h1>Nu detineti permisiuni.</h1>

    return (<>
        <h1>Useri </h1>

        <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4">
            <div class="input-group">
            <input type="search" placeholder="Cauta un user dupa nume" aria-describedby="button-addon1" class="form-control border-0 bg-light" 
                onChange={filterUsers}/>
            </div>
        </div>

        {
                userElements.filter(filterByUserName).map( (user) => (<>
                    <>
                    <Form style={{ width:'75%', border:'1px solid' }}>
                        <Form.Group as={Row}>
                            <Form.Label column sm="2">
                            Nume
                            </Form.Label>
                            <Col sm="10">
                            <Form.Control type="text" plaintext defaultValue={user.fullName} onChange={(e) => user.fullName=e.target.value}/>
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label column sm="2">
                            Email
                            </Form.Label>
                            <Col sm="10">
                            <Form.Control type="text" plaintext defaultValue={user.email} onChange={(e) => user.email=e.target.value}/>
                            </Col>
                        </Form.Group>

                        <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Label>Role</Form.Label>
                            <Form.Control as="select" defaultValue={user.role} onChange={(e) => user.role=e.target.value}>
                            <option>CLIENT</option>
                            <option>ANTREPRENOR</option>
                            <option>ADMIN</option>
                            </Form.Control>
                        </Form.Group>
                        
                        <Button variant="warning" onClick={e => editUser(user)}>Modifica</Button>
                        <Button variant="danger" onClick={e => deleteUser(user)}>Sterge</Button>

                        {user.success == true  && <a>Success !</a>}
                        {user.success == false && <a>Eroare la adaugare !</a>}

                        {user.success == "deleted"  && <a>Sters !</a>}
                        {user.success == "not_deleted"  && <a>Eroare la stergere !</a>}
                    </Form>
                    <br></br>
        </>
                </>))
        }

        {err && <h3>Eroare la actualizarea datelor de pe server.</h3>}

        

    </>)
}