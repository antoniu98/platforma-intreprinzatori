import React, {useState} from 'react';

import useUser from "../../hooks/userHook"

export default function Statistics() {
    const [rendered, setRender] = useState(false)
    const {user} = useUser()

    const toRender = () => {return(<>
        <h1>Vizualizari in functie de timp</h1>
        <div id="widgetIframe"><iframe width="100%" height="350" src="https://proiectpw.matomo.cloud/index.php?module=Widgetize&action=iframe&containerId=VisitOverviewWithGraph&disableLink=0&widget=1&moduleToWidgetize=CoreHome&actionToWidgetize=renderWidgetContainer&idSite=1&period=day&date=today&disableLink=1&widget=1" scrolling="yes" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>

        <h1>Vedere generala</h1>
        <div id="widgetIframe"><iframe width="100%" height="350" src="https://proiectpw.matomo.cloud/index.php?module=Widgetize&action=iframe&disableLink=0&widget=1&moduleToWidgetize=Live&actionToWidgetize=getSimpleLastVisitCount&idSite=1&period=day&date=today&disableLink=1&widget=1" scrolling="yes" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>

        {/* <h1>Evaluare a metricilor de performanta</h1>
        <div id="widgetIframe"><iframe width="100%" height="350" src="https://proiectpw.matomo.cloud/index.php?module=Widgetize&action=iframe&disableLink=0&widget=1&moduleToWidgetize=DevicesDetection&actionToWidgetize=getModel&idSite=1&period=day&date=today&disableLink=1&widget=1" scrolling="yes" frameborder="0" marginheight="0" marginwidth="0"></iframe></div> */}

        <h1>Reintoarceri la pagina</h1>
        <div id="widgetIframe"><iframe width="100%" height="350" src="https://proiectpw.matomo.cloud/index.php?module=Widgetize&action=iframe&forceView=1&viewDataTable=graphEvolution&disableLink=0&widget=1&moduleToWidgetize=VisitFrequency&actionToWidgetize=getEvolutionGraph&idSite=1&period=day&date=today&disableLink=1&widget=1" scrolling="yes" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>

        <h1>Vizitatori in functie de ziua saptamanii</h1>
        <div id="widgetIframe"><iframe width="100%" height="350" src="https://proiectpw.matomo.cloud/index.php?module=Widgetize&action=iframe&disableLink=0&widget=1&moduleToWidgetize=VisitTime&actionToWidgetize=getByDayOfWeek&idSite=1&period=day&date=today&disableLink=1&widget=1" scrolling="yes" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
    </>)}
    
        if(JSON.parse(user).role != "ADMIN") return <h1>Nu aveti drepturi de a accesa.</h1>

    return (<>
        <button onClick={() => setRender(true)}> Incarca statisticile</button>

        {rendered && toRender()}
    </>)
}