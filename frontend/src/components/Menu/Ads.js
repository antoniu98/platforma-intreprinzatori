import React, { useState, useEffect } from 'react';
import { ListGroup, Card, Button } from 'react-bootstrap'
import { Form, Col, Row} from 'react-bootstrap';
import { Link } from "react-router-dom"
import axios from "axios"

import useToken from '../../hooks/tokenHook'
import useUser from '../../hooks/userHook'

async function getAds(token, setAds, setErr) {
    console.log(`[getAds]Token is: ${token}`)

    await axios.get("http://localhost:8888/api/ads", { headers: {token: token}})
    .then(response => {
        setAds( response.data?.ads)
        console.log(`[getAds]: Set ads ${response.data?.ads}`)
    })
    .catch(err => {
        console.log(`[getAds]: Eroare pentru tokenul ${token}`)
        setErr(err)
    })
}

export default function Ads() {
    const [adElements, setAds] = useState([])
    const [whatToContain, setWhat] = useState('')
    const [err, setErr] = useState()

    const {token} = useToken()
    const {user} = useUser()

    const filterByAdName = (ad) => ad.name.includes(whatToContain)

    useEffect( async () => {
        await getAds(token, setAds, setErr)
    }, []) // did mount

    const filterAds = (event) => {
        event.preventDefault()
        setWhat(event.target.value)
    }

    const createHREF = (ad) => {
        return `mailto:${ad.email}?subject=[Antrep] Anuntul:${ad.name}`
    }



    /* ADMIN  */
    const editAd = async (ad) => {
        const data = {
            token: token,
            enterpriseName: ad.enterpriseName,
            adName: ad.name,
            description: ad.description,
            email: ad.email
        }
        await axios.put(`http://localhost:8888/api/ads/${ad._id}`, data)
        .then(response => {
            console.log(`[editAdd]: success ${ad.name}`)
            ad.success = true
            setErr(false)
        })
        .catch(err => {
            ad.success = false
            setErr(true)
            console.log(`[editAd]: Eroare ${err}`)
        })
    }

    const deleteAd = async (ad) => {
        await axios.delete(`http://localhost:8888/api/ads/${ad._id}`, { headers: {token:token}})
        .then(response => {
            console.log(`[deleteAd]: success ${ad.name}`)
            ad.success = "deleted"
            setErr(false)
        })
        .catch(err => {
            ad.success = "not_deleted"
            setErr(true)
            console.log(`[deleteAd]: Eroare ${err}`)
        })
    }

    const ADMIN_VIEW = (ad) => {
        return (<>
                    <Form style={{ width:'75%', border:'1px solid' }}>
                        <Form.Group as={Row}>
                            <Form.Label column sm="2">
                            Nume anunt
                            </Form.Label>
                            <Col sm="10">
                            <Form.Control type="text" plaintext defaultValue={ad.name} onChange={(e) => ad.name=e.target.value}/>
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row}>
                            <Form.Label column sm="2">
                            Descriere anunt
                            </Form.Label>
                            <Col sm="10">
                            <Form.Control type="text" plaintext defaultValue={ad.description} onChange={(e) => ad.description=e.target.value}/>
                            </Col>
                        </Form.Group>
                        
                        <Button variant="warning" onClick={e => editAd(ad)}>Modifica</Button>
                        <Button variant="danger" onClick={e => deleteAd(ad)}>Sterge</Button>

                        {ad.success == true  && <a>Success !</a>}
                        {ad.success == false && <a>Eroare la adaugare !</a>}

                        {ad.success == "deleted"  && <a>Sters !</a>}
                        {ad.success == "not_deleted"  && <a>Eroare la stergere !</a>}
                    </Form>
        </>)
    }

    const NON_ADMIN_VIEW = (ad) => {
        return (<>
                    <Card style={{ width: '75%' }}>
                    <Card.Body>
                            <Card.Title>{ad.name}</Card.Title>
                            <Card.Text style={{ width: '80%'}}>
                            Descriere: {ad.description}
                            </Card.Text>
                            <Card.Text>
                            Postat de : {ad.enterpriseName}
                            </Card.Text>
                            <Button variant="primary" href={createHREF(ad)}>Trimite email: {ad.email}</Button>
                    </Card.Body>
                    </Card>
        </>)
    }

    return (<>
        <h1>Anunturi <Link to="/add-ad" className="btn btn-primary">Adauga anunt</Link> </h1>

        <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4">
            <div class="input-group">
            <input type="search" placeholder="Cauta un anunt dupa nume" aria-describedby="button-addon1" class="form-control border-0 bg-light" 
                onChange={filterAds}/>
            </div>
        </div>

        {
                adElements.filter(filterByAdName).map( (ad) => (<>
                    {   JSON.parse(user).role == "ADMIN" ?  
                        ADMIN_VIEW(ad) : (JSON.parse(user).role == "ANTREPRENOR" ? // esti enterprise: poti edita doar ce e al tau
                                            (ad.enterpriseName == JSON.parse(user).fullName ? ADMIN_VIEW(ad) : NON_ADMIN_VIEW(ad) )
                                            : NON_ADMIN_VIEW(ad)
                                         )
                    }
                </>))
        }

        {err && <h3>Eroare la actualizarea datelor de pe server.</h3>}

        

    </>)
}