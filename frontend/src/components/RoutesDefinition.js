import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Ads from "./Menu/Ads"
import Profile from "./Menu/Profile"
import Users from "./Menu/Users"
import Statistics from "./Menu/Statistics"
import AddAd from "./Menu/AddAd"


export default function RoutesDefinition() {
    return (<>
    {/* <div className="outer"> */}
    <div className="inner w-75">
        <Switch>
            <Route exact path='/'>
              <h1>Welcome</h1>
            </Route>
            
            <Route path="/ads">
              <Ads />
            </Route>

            <Route path="/users">
              <Users />
            </Route>

            <Route path="/profile">
              <Profile />
            </Route>

            <Route path="/statistics">
              <Statistics />
            </Route>

            <Route path="/add-ad">
              <AddAd />
            </Route>
        </Switch>
    </div>
    {/* </div> */}
    </>)
}