"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const express_1 = __importDefault(require("express"));
const userController_1 = require("./userController");
const enterpriseController_1 = require("./enterpriseController");
const adsController_1 = require("./adsController");
const authController_1 = require("./authController");
const jwtAuthorizer_1 = require("../middlewares/authorization/jwtAuthorizer");
exports.router = express_1.default.Router();
exports.router.use('/users', jwtAuthorizer_1.authorizeTokenAndExtractUser, userController_1.userRouter);
exports.router.use('/firma', jwtAuthorizer_1.authorizeTokenAndExtractUser, enterpriseController_1.enterpriseRouter);
exports.router.use('/ads', jwtAuthorizer_1.authorizeTokenAndExtractUser, adsController_1.adsRouter);
exports.router.use('/auth', authController_1.authRouter);
