"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.adsRouter = void 0;
const express_1 = __importDefault(require("express"));
const product_1 = require("../models/product");
const serverError_1 = require("../utils/serverError");
const roleAuthorizer_1 = require("../middlewares/authorization/roleAuthorizer");
/* PRODUCT e acelasi lucru cu ADS -> trebuie redenumite */
exports.adsRouter = express_1.default.Router();
class AdsService {
    /* GET */
    async getAllAds() {
        try {
            return await product_1.ProductModel.find();
        }
        catch (err) {
            console.log(`\n[AdsService.getAllAds]: ${err}`);
            throw new serverError_1.ServerError('Eroare la gasirea tuturor anunturilor', 401);
        }
    }
    async getAdById(id) {
        try {
            return await product_1.ProductModel.findOne({
                _id: id
            });
        }
        catch (err) {
            console.log(`\n[AdsService.getAdById]: ${err}`);
            throw new serverError_1.ServerError(`Eroare la gasirea anuntului cu id:${id}`, 401);
        }
    }
    async getAllAdsByFilter(enterpriseName, name) {
        try {
            return await product_1.ProductModel.find({
                ...(enterpriseName && { enterpriseName: enterpriseName }),
                ...(name && { name: name }),
            });
        }
        catch (err) {
            console.log(`\n[AdsService.getAllAdsByFilter]: ${err}`);
            throw new serverError_1.ServerError('Eroare la filtrarea anunturilor', 401);
        }
    }
    /* CREATE, EDIT */
    async createAd(enterpriseName, adName, description, email) {
        try {
            const adModel = new product_1.ProductModel({
                enterpriseName: enterpriseName,
                name: adName,
                description: description,
                email: email
            });
            return await adModel.save();
        }
        catch (err) {
            console.log(`\n[AdsService.createAd]: ${err}`);
            throw new serverError_1.ServerError('Eroare la adaugare anunt', 401);
        }
    }
    async updateAdById(id, enterpriseName, name, description, email) {
        try {
            return await product_1.ProductModel.updateOne({
                _id: id
            }, {
                ...(enterpriseName && { enterpriseName: enterpriseName }),
                ...(name && { name: name }),
                ...(description && { description: description }),
                ...(email && { email: email })
            });
        }
        catch (err) {
            console.log(`\n[AdsService.updateAdById]: ${err}`);
            throw new serverError_1.ServerError(`Eroare la editarea anuntului id ${id}`, 401);
        }
    }
    /* DELETE */
    async deleteAdById(id) {
        try {
            return await product_1.ProductModel.deleteOne({
                _id: id
            });
        }
        catch (err) {
            console.log(`\n[AdsService.deleteAdById]: ${err}`);
            throw new serverError_1.ServerError(`Eroare la stergerea anuntului cu id:${id}`, 401);
        }
    }
}
class AdsController {
    /* GET */
    static async getAllAds(req, res, next) {
        try {
            const ads = await AdsController.adsService.getAllAds();
            res.send({ ads: ads });
        }
        catch (err) {
            next(err);
        }
    }
    static async getAdById(req, res, next) {
        try {
            const { id } = req.params;
            const ad = await AdsController.adsService.getAdById(id);
            res.send({ ads: ad });
        }
        catch (err) {
            next(err);
        }
    }
    static async getAdByFilter(req, res, next) {
        try {
            const { enterpriseName, name } = req.body;
            const ads = await AdsController.adsService.getAllAdsByFilter(enterpriseName, name);
            res.send({ ads: ads });
        }
        catch (err) {
            next(err);
        }
    }
    static async getOwnAds(req, res, next) {
        try {
            const enterpriseName = req.body.user.name;
            const ads = await AdsController.adsService.getAllAdsByFilter(enterpriseName, null);
            res.send({ ads: ads });
        }
        catch (err) {
            next(err);
        }
    }
    /* CREATE, EDIT */
    static async createAd(req, res, next) {
        try {
            const { enterpriseName, adName, description, email } = req.body;
            const confirmation = await AdsController.adsService.createAd(enterpriseName, adName, description, email);
            res.send({ ads: confirmation });
        }
        catch (err) {
            next(err);
        }
    }
    static async editAdById(req, res, next) {
        try {
            const { id } = req.params;
            const { enterpriseName, adName, description, email } = req.body;
            const response = await AdsController.adsService.updateAdById(id, enterpriseName, adName, description, email);
            res.send({ ads: response });
        }
        catch (err) {
            next(err);
        }
    }
    /* DELETE */
    static async deleteAdById(req, res, next) {
        try {
            const { id } = req.params;
            const result = await AdsController.adsService.deleteAdById(id);
            res.send({ ads: result });
        }
        catch (err) {
            next(err);
        }
    }
}
AdsController.adsService = new AdsService();
/* GET */
exports.adsRouter.get('/', roleAuthorizer_1.RoleAuthorizer.anyRole, AdsController.getAllAds);
exports.adsRouter.get('/find/:id', roleAuthorizer_1.RoleAuthorizer.anyRole, AdsController.getAdById);
exports.adsRouter.get('/filter', roleAuthorizer_1.RoleAuthorizer.anyRole, AdsController.getAdByFilter);
exports.adsRouter.get('/my-ads', roleAuthorizer_1.RoleAuthorizer.enterpriserRole, AdsController.getOwnAds);
/* CREATE, EDIT */
exports.adsRouter.post('/', roleAuthorizer_1.RoleAuthorizer.enterpriserRole, AdsController.createAd);
exports.adsRouter.put('/:id', roleAuthorizer_1.RoleAuthorizer.enterpriserRole, AdsController.editAdById);
/* DELETE */
exports.adsRouter.delete('/:id', roleAuthorizer_1.RoleAuthorizer.enterpriserRole, AdsController.deleteAdById);
