"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.productRouter = void 0;
const express_1 = __importDefault(require("express"));
const product_1 = require("../models/product");
exports.productRouter = express_1.default.Router();
exports.productRouter.post('/', async (req, res) => {
    const product = new product_1.ProductModel({});
    try {
        const newProduct = await product.save();
        return res.send('new product added');
    }
    catch (err) {
        return res.send('erorr: ' + err.message);
    }
});
