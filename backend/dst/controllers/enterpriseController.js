"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.enterpriseRouter = void 0;
const express_1 = __importDefault(require("express"));
const enterprise_1 = require("../models/enterprise");
exports.enterpriseRouter = express_1.default.Router();
exports.enterpriseRouter.post('/', async (req, res) => {
    const enterprise = new enterprise_1.EnterpriseModel({
        name: "firma mea",
        owner: "Anto",
        details: {
            registrationDate: new Date(),
            site: "www.firmamea.ro"
        }
    });
    try {
        const newEnterprise = await enterprise.save();
        return res.send('new enterprise added');
    }
    catch (err) {
        return res.send('erorr: ' + err.message);
    }
});
