"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authRouter = void 0;
const express_1 = __importDefault(require("express"));
const user_1 = require("../models/user");
const serverError_1 = require("../utils/serverError");
const mailConnection_1 = require("../config/mailConnection");
const passwordHasher_1 = require("../utils/passwordHasher");
const tokenService = __importStar(require("../utils/tokenService"));
class AuthService {
    // REGISTER
    async hashPassword(password) {
        try {
            return await passwordHasher_1.hashPassword(password);
        }
        catch (err) {
            console.log(`\n[hashPassword]: eroare la hashingul parolei: ${err}`);
            throw new serverError_1.ServerError('Eroare. Reincercati inregistrarea', 401);
        }
    }
    async checkUserInDB(email) {
        let user;
        try {
            user = await user_1.UserModel.findOne({ email: email });
        }
        catch (err) {
            console.log(`\n[addUserToDb]: Eroare la verificarea userului in BD: ${err}`);
        }
        if (user)
            if (user.confirmed)
                throw new serverError_1.ServerError('Aceasta adresa de mail este deja inregistrata.', 401);
            else { //e inregistrat, POATE a expirat linkul si vrea sa se inregistreze iar
                await user_1.UserModel.deleteOne({ email: email });
                return false;
            }
        else
            return false;
    }
    async addUserToDb(email, fullName, hashedPassword, role = "CLIENT") {
        if (role != "CLIENT" && role != "ANTREPRENOR")
            throw new serverError_1.ServerError("Rolul declarat este invalid.", 401);
        const user = new user_1.UserModel({
            email: email,
            fullName: fullName,
            password: hashedPassword,
            role: role,
            confirmed: false
        });
        try {
            await user.save();
        }
        catch (err) {
            console.log(`\n[addUserToDb]: Eroare la adaugarea userului in BD: ${err}`);
            throw new serverError_1.ServerError('Eroare la adaugarea in baza de date', 401);
        }
    }
    async createToken(email) {
        try {
            return await tokenService.createExpirationJwtToken({
                email: email
            });
        }
        catch (err) {
            throw new serverError_1.ServerError("Eroare la crearea tokenului!", 401);
        }
    }
    async sendEmailTo(token, email) {
        console.log(`\n[sendEmailTo]: Start sendEmailTo().`);
        const url = `http://localhost:8888/api/auth/confirmation/${token}`;
        const options = {
            from: "antoniu.neacsu@stud.acs.upb.ro",
            to: email,
            subject: "Confirmarea inregistrarii.",
            html: `Apasati <a href="${url}">aici</a> in maximum 15 minute pentru a confirma inregistrarea.`,
        };
        const info = mailConnection_1.sendMail(options);
        if (info == "error") {
            throw new serverError_1.ServerError("Eroare la trimiterea mailului de confirmare", 401);
        }
        else
            console.log(`\n[sendEmailTo]: Mail was sent and received with info: ${info}`);
    }
    async undoUserSave(email) {
        console.log(`\n[undoUserSave]: Start undoUserSave().`);
        await user_1.UserModel.deleteOne({ email: email }, (err) => {
            if (err)
                console.log(`\n[undoUserSave]: Eroare la stergerea userului: ${err}`);
        });
    }
    // LOGIN
    async findUser(email, password) {
        /* 1. Get the user from db*/
        const user = await user_1.UserModel.findOne({
            'email': email
        }, (err, result) => {
            if (err) {
                throw new serverError_1.ServerError(`error: Nu exista un user cu aceasta adresa de mail: ${email}`, 401);
            }
        });
        /* 2. Check password hash */
        if (!user)
            throw new serverError_1.ServerError(`Nu exista un user cu aceasta adresa de mail: ${email}`, 401);
        const correctPassword = await passwordHasher_1.comparePassword(password, user.password);
        if (!correctPassword)
            throw new serverError_1.ServerError(`Parola introdusa este gresita.`, 401);
        return user;
    }
    async checkPassword(password, hashedPassword) {
        try {
            const correct = await passwordHasher_1.comparePassword(password, hashedPassword);
            if (!correct)
                throw new serverError_1.ServerError("Parola nu este corecta", 401);
        }
        catch (err) {
            throw new serverError_1.ServerError("Eroare la verificarea parolei.", 401);
        }
    }
    async generateJwtLoginToken(data) {
        try {
            return await tokenService.createJwtToken(data);
        }
        catch (err) {
            throw new serverError_1.ServerError("Eroare la generarea tokenului jwt.", 401);
        }
    }
    // CONFIRMATION
    async decryptRegistrationToken(token) {
        try {
            return await tokenService.verifyAndDecodeExpirationJwtToken(token);
        }
        catch (err) {
            throw new serverError_1.ServerError("Tokenul este posibil sa fi expirat. Reperati inregistrarea.", 401);
        }
    }
    async confirmRegistrationForEmail(email) {
        try {
            const user = await user_1.UserModel.findOne({ email: email });
            if (!user)
                throw new serverError_1.ServerError("User to be confirmed not found.", 401);
            /* update 'confirmed' field to 'true */
            user.confirmed = true;
            await user_1.UserModel.updateOne({ email: email }, user);
        }
        catch (err) {
            console.log(`[confirmRegistrationForUser]: Confirming user reg in db failed: ${err}`);
        }
    }
}
class AuthController {
    static async login(req, res, next) {
        // TODO: check and serialize parameters
        try {
            /* 1. get user by email from db */
            let { email, password } = req.body;
            let user = await AuthController.authService.findUser(email, password);
            /* 2. check password: if passw is wrong it will throw an error */
            await AuthController.authService.checkPassword(password, user.password);
            /* 3. Check that the mail is confirmed */
            if (!user.confirmed) {
                throw new serverError_1.ServerError("Contul nu este confirmat", 401);
            }
            /* 4. generate jwt token */
            const token = await AuthController.authService.generateJwtLoginToken({
                id: user._id,
                email: user.email,
                fullName: user.fullName,
                role: user.role
            });
            /* 5. return the jwt token */
            res.send({
                token: token,
                confirmed: true,
                user: {
                    id: user._id,
                    email: user.email,
                    fullName: user.fullName,
                    role: user.role
                }
            });
        }
        catch (err) {
            next(err);
        }
    }
    static async register(req, res, next) {
        // TODO: check and serialize parameters
        const successString = "Inregistrarea a avut loc cu succes.\n" +
            "Accesati mail-ul pentru a confirma contul si a avea acces la platforma.";
        try {
            const { email, fullName, password, role } = req.body;
            /* 1. cripteaza parola */
            const hashedPassword = await AuthController.authService.hashPassword(password);
            /* 2. check user in db: it will throw an error if it does not exist*/
            await AuthController.authService.checkUserInDB(email);
            /* 3. create token */
            let token = await AuthController.authService.createToken(email);
            /* 4. Add user in db */
            await AuthController.authService.addUserToDb(email, fullName, hashedPassword, role);
            /* 5. send verification email */
            await AuthController.authService.sendEmailTo(token, email);
            /* 6. Send message */
            res.send({
                message: successString
            });
        }
        catch (err) {
            next(err);
        }
    }
    static async confirmation(req, res, next) {
        const { token } = req.params;
        try {
            /* 1. Decrypt token and get the user fields */
            const expirationToken = await AuthController.authService.decryptRegistrationToken(token);
            const email = expirationToken.email;
            /* Update in db 'confirmation field' -> true */
            await AuthController.authService.confirmRegistrationForEmail(email);
            res.send({
                confirmed: true
            });
        }
        catch (err) {
            next(err);
        }
    }
}
AuthController.authService = new AuthService();
exports.authRouter = express_1.default.Router();
exports.authRouter.post('/login', AuthController.login);
exports.authRouter.post('/register', AuthController.register);
exports.authRouter.get('/confirmation/:token', AuthController.confirmation);
