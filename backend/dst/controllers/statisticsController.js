"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRouter = void 0;
const express_1 = __importDefault(require("express"));
const user_1 = require("../models/user");
exports.userRouter = express_1.default.Router();
exports.userRouter.get('/:email', async (req, res) => {
    let { email } = req.params;
    user_1.UserModel.findOne({ email: email }, (err, result) => {
        return res.json(result);
    });
});
exports.userRouter.get('/', async (req, res) => {
    user_1.UserModel.find((err, result) => {
        return res.json(result);
    });
});
exports.userRouter.post('/', async (req, res) => {
    const user = new user_1.UserModel({
        email: req.body.email,
        fullName: req.body.fullName,
        role: req.body.role,
        password: req.body.password,
        firme: req.body.firme
    });
    try {
        const newUser = await user.save();
        return res.json(newUser);
    }
    catch (err) {
        return res.send('erorr: ' + err.message);
    }
});
