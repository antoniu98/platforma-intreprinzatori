"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRouter = void 0;
const express_1 = __importDefault(require("express"));
const user_1 = require("../models/user");
const serverError_1 = require("../utils/serverError");
const roleAuthorizer_1 = require("../middlewares/authorization/roleAuthorizer");
/* PRODUCT e acelasi lucru cu ADS -> trebuie redenumite */
exports.userRouter = express_1.default.Router();
class UserService {
    /* GET */
    async getAllUsers() {
        try {
            return await user_1.UserModel.find();
        }
        catch (err) {
            console.log(`\n[ServiceService.getAllUsers]: ${err}`);
            throw new serverError_1.ServerError('Eroare la gasirea tuturor userilor', 401);
        }
    }
    async getUserById(id) {
        try {
            return await user_1.UserModel.find({ _id: id });
        }
        catch (err) {
            console.log(`\n[ServiceService.getUserById]: ${err}`);
            throw new serverError_1.ServerError(`Eroare la gasirea userului ${id}`, 401);
        }
    }
    async getUserByEmail(email) {
        try {
            return await user_1.UserModel.find({ email: email });
        }
        catch (err) {
            console.log(`\n[ServiceService.getUserByEmail]: ${err}`);
            throw new serverError_1.ServerError(`Eroare la gasirea userului ${email}`, 401);
        }
    }
    /* CREATE, EDIT */
    async editUserById(id, data) {
        try {
            return await user_1.UserModel.updateOne({
                _id: id
            }, data);
        }
        catch (err) {
            console.log(`\n[AdsService.editUserById]: ${err}`);
            throw new serverError_1.ServerError(`Eroare la editarea userului id ${id}`, 401);
        }
    }
    /* DELETE */
    async deleteUserById(id) {
        try {
            return await user_1.UserModel.deleteOne({
                _id: id
            });
        }
        catch (err) {
            console.log(`\n[UserService.deleteUserById]: ${err}`);
            throw new serverError_1.ServerError(`Eroare la stergerea userului cu id:${id}`, 401);
        }
    }
}
class UserController {
    // GET
    static async getAllUsers(req, res, next) {
        try {
            const result = await UserController.userService.getAllUsers();
            res.send({ users: result });
        }
        catch (err) {
            next(err);
        }
    }
    static async getOwnUser(req, res, next) {
        try {
            const { id } = req.body.user;
            const result = await UserController.userService.getUserById(id);
            res.send({ users: result });
        }
        catch (err) {
            next(err);
        }
    }
    // CREATE / EDIT
    static async editOwnUser(req, res, next) {
        try {
            const { id } = req.params;
            const { email, fullName } = req.body;
            const data = {
                email: email,
                fullName: fullName
            };
            const result = await UserController.userService.editUserById(id, data);
            res.send({ users: result });
        }
        catch (err) {
            next(err);
        }
    }
    static async editUserByID(req, res, next) {
        try {
            const { id } = req.params;
            const { email, fullName, role } = req.body;
            const data = {
                ...(email && { email: email }),
                ...(fullName && { fullName: fullName }),
                ...(role && { role: role }),
            };
            const result = await UserController.userService.editUserById(id, data);
            res.send({ users: result });
        }
        catch (err) {
            next(err);
        }
    }
    // DELETE
    static async deleteUserById(req, res, next) {
        try {
            const { id } = req.params;
            const result = await UserController.userService.deleteUserById(id);
            res.send({ users: result });
        }
        catch (err) {
            next(err);
        }
    }
}
UserController.userService = new UserService();
// GET
exports.userRouter.get('/', roleAuthorizer_1.RoleAuthorizer.adminRole, UserController.getAllUsers);
exports.userRouter.get('/own', roleAuthorizer_1.RoleAuthorizer.anyRole, UserController.getOwnUser);
// CREATE / EDIT
// userRouter.put   ('/own',      RoleAuthorizer.anyRole,    UserController.editOwnUser)
exports.userRouter.put('/:id', roleAuthorizer_1.RoleAuthorizer.adminRole, UserController.editUserByID);
// DELETE
exports.userRouter.delete('/:id', roleAuthorizer_1.RoleAuthorizer.adminRole, UserController.deleteUserById);
// daca se modifica rolul trebuie dat login iar
