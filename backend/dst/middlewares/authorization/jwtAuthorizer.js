"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authorizeTokenAndExtractUser = void 0;
const tokenService_1 = require("../../utils/tokenService");
const serverError_1 = require("../../utils/serverError");
const authorizeTokenAndExtractUser = async (req, res, next) => {
    // TODO : add functionality
    try {
        console.log(`[auth]: checking req ${JSON.stringify(req.body)}`);
        if (!req.body.token && !req.headers.token)
            throw new serverError_1.ServerError("Lipseste tokenul pentru autentificare!", 401);
        let { token } = req.body;
        if (!token)
            token = req.header('token');
        if (!token)
            console.log(`[authorize]: no token`);
        const user = await tokenService_1.verifyAndDecodeJwtToken(token);
        req.body.user = user;
        console.log(`Decoded token: ${JSON.stringify(user)}`);
        next();
    }
    catch (err) {
        next(err);
    }
};
exports.authorizeTokenAndExtractUser = authorizeTokenAndExtractUser;
