"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleAuthorizer = void 0;
const serverError_1 = require("../../utils/serverError");
const ROLES = {
    CLIENT: "CLIENT",
    ANTREPRENOR: "ANTREPRENOR",
    ADMIN: "ADMIN"
};
class RoleAuthorizer {
}
exports.RoleAuthorizer = RoleAuthorizer;
RoleAuthorizer.authorizeRoles = (req, res, next, ...roles) => {
    try {
        for (let i = 0; i < roles.length; i++) {
            // previous middleware will inject "user" obj in req.body
            if (req.body.user.role == roles[i]) {
                return;
            }
        }
    }
    catch (err) {
        throw new serverError_1.ServerError('Eroare la verificarea rolului dvs.', 403);
    }
    throw new serverError_1.ServerError('Nu sunteti autorizat sa accesati resursa!', 403);
};
RoleAuthorizer.anyRole = (req, res, next) => {
    try {
        RoleAuthorizer.authorizeRoles(req, res, next, ROLES.CLIENT, ROLES.ANTREPRENOR, ROLES.ADMIN);
        next();
    }
    catch (err) {
        next(err);
    }
};
RoleAuthorizer.enterpriserRole = (req, res, next) => {
    try {
        RoleAuthorizer.authorizeRoles(req, res, next, ROLES.ANTREPRENOR, ROLES.ADMIN);
        next();
    }
    catch (err) {
        next(err);
    }
};
RoleAuthorizer.adminRole = (req, res, next) => {
    try {
        RoleAuthorizer.authorizeRoles(req, res, next, ROLES.ADMIN);
        next();
    }
    catch (err) {
        next(err);
    }
};
//
// const authorizeRoles = (...roles) => {
//     return (req, res, next) => {
//         for (let i = 0; i < roles.length; i++) {
//             // TODO: in functie de implemetare
//             // verificati daca req.user contine role sau userRole
//             if (req.user.userRole === roles[i]) { // observati cum in req exista obiectul user trimis la middlewareul anterior, de autorizare token
//                     return next();
//             }
//         }
//         throw new ServerError('Nu sunteti autorizat sa accesati resursa!', 403);
//     }
// };
// const AnyRoleCheck = () => AuthorizationFilter.authorizeRoles(RoleConstants.USER, RoleConstants.MANAGER, RoleConstants.ADMIN);
// const ManagerRoleCheck = () => AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER, RoleConstants.ADMIN);
// const AdminRoleCheck = () => AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN);
