"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authorizeTokenAndExtractUser = void 0;
const tokenService_1 = require("../../../utils/tokenService");
const serverError_1 = require("../../../utils/serverError");
const authorizeTokenAndExtractUser = async (req, res, next) => {
    // TODO : add functionality
    if (!req.body.token)
        throw new serverError_1.ServerError("Lipseste tokenul pentru autentificare!", 401);
    const { token } = req.body;
    try {
        const user = await tokenService_1.verifyAndDecodeJwtToken(token);
        req.body.user = user;
        console.log(`Decoded token: ${JSON.stringify(user)}`);
        next();
    }
    catch (err) {
        next(err);
    }
};
exports.authorizeTokenAndExtractUser = authorizeTokenAndExtractUser;
