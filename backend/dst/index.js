"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const helmet_1 = __importDefault(require("helmet"));
const dbConnection_1 = require("./config/dbConnection");
const mailConnection_1 = require("./config/mailConnection");
const router_1 = require("./controllers/router");
const cors = require('cors');
const errorMiddleware = (err, req, res, next) => {
    if (err) {
        console.log(`err: ${err}`);
        res.status(err.httpStatusCode);
        res.send({ err: err.message });
    }
    else {
        next(err);
    }
};
/* middleware */
const app = express_1.default();
let whitelist = ['http://localhost:3000'];
app.use(cors({
    origin: function (origin, callback) {
        // allow requests with no origin 
        if (!origin)
            return callback(null, true);
        if (whitelist.indexOf(origin) === -1) {
            var message = 'The CORS policy for this origin doesnt ' +
                'allow access from the particular origin.';
            return callback(new Error(message), false);
        }
        return callback(null, true);
    }
}));
app.use(helmet_1.default());
app.use(morgan_1.default(':remote-addr - :remote-user [:date[web]] ":method :url HTTP/:http-version" :status :res[content-length]'));
app.use(express_1.default.json());
app.use('/api', router_1.router);
app.use(errorMiddleware);
/* DB connect */
dbConnection_1.connectToDatabase();
/* Mail init */
mailConnection_1.initMailer();
const port = process.env.PORT || 8888;
app.listen(port, () => {
    console.log(`App is listening on ${port}`);
});
