"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectToDatabase = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
// TODO : put password in env
const url = 'mongodb+srv://anto:anto@cluster-0.tfnd6.mongodb.net/antreprenori-db?retryWrites=true&w=majority';
function connectToDatabase() {
    mongoose_1.default
        .connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
        .catch((err) => console.log(err.reason));
    const db = mongoose_1.default.connection;
    db.on('error', console.error.bind(console, 'Connection error:'));
    db.once('open', () => {
        console.log('Connected to MongoDB');
    });
}
exports.connectToDatabase = connectToDatabase;
