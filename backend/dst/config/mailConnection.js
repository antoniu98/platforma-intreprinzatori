"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendMail = exports.initMailer = void 0;
const nodemailer_1 = __importDefault(require("nodemailer"));
let transporder;
function initMailer() {
    transporder = nodemailer_1.default.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
            user: "3f6b443d5f0b40",
            pass: "f0a1a90caf8387"
        }
    });
    console.log(`Nodemailer init done.`);
}
exports.initMailer = initMailer;
const sendMail = (options) => {
    let response = "";
    transporder.sendMail(options, (err, info) => {
        if (err) {
            console.log(`\n[sendMail]: Error at sending mail : ${err}`);
            response = "error";
            return;
        }
        console.log(`\n[sendMail]: Sent mail with info: ${JSON.stringify(info)}`);
        response = info;
    });
    return response;
};
exports.sendMail = sendMail;
