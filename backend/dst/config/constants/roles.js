"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROLE = void 0;
var ROLE;
(function (ROLE) {
    ROLE["ADMIN"] = "ADMIN";
    ROLE["ANTREPRENOR"] = "ANTREPRENOR";
    ROLE["CLIENT"] = "CLIENT";
})(ROLE = exports.ROLE || (exports.ROLE = {}));
