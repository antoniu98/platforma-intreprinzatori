"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnterpriseModel = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const enterpriseSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    owner: {
        type: String,
        required: true
    },
    details: {
        registrationDate: Date,
        description: String,
        site: String
    }
});
exports.EnterpriseModel = mongoose_1.default.model('Enterprise', enterpriseSchema);
