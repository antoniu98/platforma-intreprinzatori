"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDTO = exports.UserLoginDTO = void 0;
class UserLoginDTO {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
    get Email() { return this.email; }
    get Password() { return this.password; }
}
exports.UserLoginDTO = UserLoginDTO;
class UserDTO {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
    get Email() { return this.email; }
    get Password() { return this.password; }
}
exports.UserDTO = UserDTO;
