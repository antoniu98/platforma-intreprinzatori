"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyAndDecodeExpirationJwtToken = exports.createExpirationJwtToken = exports.verifyAndDecodeJwtToken = exports.createJwtToken = void 0;
const serverError_1 = require("./serverError");
const jwt = require('jsonwebtoken');
const secret = "change_this_secret";
const options = {
    issuer: "Anto",
    subject: "Verification token", //process.env.JWT_SUBJECT,
};
const optionsExpirationToken = { ...options, expiresIn: '15m' };
/* Session token */
async function createJwtToken(data) {
    return await jwt.sign(data, secret, options);
}
exports.createJwtToken = createJwtToken;
async function verifyAndDecodeJwtToken(token) {
    try {
        return await jwt.verify(token, secret, options);
    }
    catch (err) {
        console.trace(err);
        throw new serverError_1.ServerError(`Eroare la decriptarea tokenului! ${err}`, 401);
    }
}
exports.verifyAndDecodeJwtToken = verifyAndDecodeJwtToken;
/* Expiration token for registration */
async function createExpirationJwtToken(data) {
    return await jwt.sign(data, secret, optionsExpirationToken);
}
exports.createExpirationJwtToken = createExpirationJwtToken;
async function verifyAndDecodeExpirationJwtToken(token) {
    try {
        return await jwt.verify(token, secret, optionsExpirationToken);
    }
    catch (err) {
        console.trace(err);
        throw new serverError_1.ServerError(`Repetati inregistrarea. Eroare la decriptarea tokenului! ${err}`, 401);
    }
}
exports.verifyAndDecodeExpirationJwtToken = verifyAndDecodeExpirationJwtToken;
