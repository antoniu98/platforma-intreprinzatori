"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.comparePassword = exports.hashPassword = void 0;
const bcryptjs_1 = __importDefault(require("bcryptjs"));
async function hashPassword(plainTextPassword) {
    const salt = await bcryptjs_1.default.genSalt();
    return await bcryptjs_1.default.hash(plainTextPassword, salt);
}
exports.hashPassword = hashPassword;
async function comparePassword(plainTextPassword, hashedPassword) {
    return await bcryptjs_1.default.compare(plainTextPassword, hashedPassword);
}
exports.comparePassword = comparePassword;
