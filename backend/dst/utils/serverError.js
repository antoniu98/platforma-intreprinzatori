"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerError = void 0;
class ServerError extends Error {
    constructor(message, httpStatus) {
        super();
        this.message = message;
        this.httpStatusCode = httpStatus;
        Error.captureStackTrace(this, this.constructor);
    }
}
exports.ServerError = ServerError;
