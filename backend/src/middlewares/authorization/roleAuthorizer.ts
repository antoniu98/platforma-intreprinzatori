import { Request, NextFunction, Response } from "express";
import { ServerError } from "../../utils/serverError"

const ROLES = {
    CLIENT: "CLIENT", 
    ANTREPRENOR: "ANTREPRENOR",
    ADMIN: "ADMIN"
}

export class RoleAuthorizer {
    private static authorizeRoles = (req: Request, res: Response, next: NextFunction, ...roles: any) => {
        try {
            for (let i = 0; i < roles.length; i++) {
                // previous middleware will inject "user" obj in req.body
                if (req.body.user.role == roles[i]) {
                        return
                }
            }
        } catch (err) {
            throw new ServerError('Eroare la verificarea rolului dvs.', 403);
        }
        throw new ServerError('Nu sunteti autorizat sa accesati resursa!', 403);
    }

    public static anyRole = (req: Request, res: Response, next: NextFunction) => { 
        try {
            RoleAuthorizer.authorizeRoles(req, res, next, ROLES.CLIENT, ROLES.ANTREPRENOR, ROLES.ADMIN)
            next()
        } catch (err) {
            next(err)
        }
    }

    public static enterpriserRole = (req: Request, res: Response, next: NextFunction) => { 
        try {
            RoleAuthorizer.authorizeRoles(req, res, next, ROLES.ANTREPRENOR, ROLES.ADMIN)
            next()
        } catch (err) {
            next(err)
        }
    }

    public static adminRole = (req: Request, res: Response, next: NextFunction) => { 
        try {
            RoleAuthorizer.authorizeRoles(req, res, next, ROLES.ADMIN)
            next()
        } catch (err) {
            next(err)
        }
    }
}


//
// const authorizeRoles = (...roles) => {
//     return (req, res, next) => {

//         for (let i = 0; i < roles.length; i++) {
//             // TODO: in functie de implemetare
//             // verificati daca req.user contine role sau userRole
//             if (req.user.userRole === roles[i]) { // observati cum in req exista obiectul user trimis la middlewareul anterior, de autorizare token
//                     return next();
//             }
//         }
//         throw new ServerError('Nu sunteti autorizat sa accesati resursa!', 403);
//     }
// };

// const AnyRoleCheck = () => AuthorizationFilter.authorizeRoles(RoleConstants.USER, RoleConstants.MANAGER, RoleConstants.ADMIN);
// const ManagerRoleCheck = () => AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER, RoleConstants.ADMIN);
// const AdminRoleCheck = () => AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN);