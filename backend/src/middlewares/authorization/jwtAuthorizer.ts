import {NextFunction, Request, Response} from "express"
import { verifyAndDecodeJwtToken } from "../../utils/tokenService"
import { ServerError} from "../../utils/serverError"

export const authorizeTokenAndExtractUser = async (req:Request, res:Response, next:NextFunction) => {   
    // TODO : add functionality
    try {
        console.log(`[auth]: checking req ${JSON.stringify(req.body)}`)

        if (!req.body.token && !req.headers.token)
            throw new ServerError("Lipseste tokenul pentru autentificare!", 401)

        let { token } = req.body
        if (!token)
            token = req.header('token')

        if (!token)
            console.log(`[authorize]: no token`)

        const user = await verifyAndDecodeJwtToken(token)
        req.body.user = user
        console.log(`Decoded token: ${JSON.stringify(user)}`)
        next()
    } catch (err) {
        next(err)
    }
}