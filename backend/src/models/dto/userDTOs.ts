export class UserLoginDTO {
    private email: String;
    private password: String;

    constructor(email: String, password: String) {
        this.email = email;
        this.password = password;
    }

    get Email() { return this.email; }
    get Password() { return this.password; }
}

export class UserDTO {
    private email: String;
    private password: String;

    constructor(email: String, password: String) {
        this.email = email;
        this.password = password;
    }

    get Email() { return this.email; }
    get Password() { return this.password; }
}