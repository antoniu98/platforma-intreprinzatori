import mongoose from 'mongoose'

interface Product {
    enterpriseName: String;
    name: String;
    description: String;
    email: String
    // poze: Image[];
    // details: String;
}



interface ProductModelInterface extends Product, mongoose.Document {}

const productSchema = new mongoose.Schema({
    enterpriseName: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
})

export const ProductModel = mongoose.model<ProductModelInterface>('product', productSchema)