import mongoose from 'mongoose'
import {ROLE} from '../config/constants/roles'

interface User {
    email: String;
    fullName: String;
    password: String;
    role: ROLE;
    // enterprises: String[];
    // register confirmation
    confirmed: Boolean;
}

interface UserModelInterface extends User, mongoose.Document {}

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true, 
        unique: true
    },
    fullName: {
        type: String,
        required: true 
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: ROLE,
        required: true 
    },
    // enterprises: [{
    //     type: String
    // }],
    confirmed: {
        type: Boolean,
        default: false
    }
})

export const UserModel = mongoose.model<UserModelInterface>('User', userSchema)