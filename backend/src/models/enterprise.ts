import mongoose from 'mongoose'

interface Enterprise {
    name: String;
    owner: String;
    details: {
        registrationDate: Date,
        description: String,
        site: String
    };
    // poza: String;
}

interface EnterpriseModelInterface extends Enterprise, mongoose.Document {}

const enterpriseSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    owner: {
        type: String,
        required: true
    },
    details: {
        registrationDate: Date,
        description: String,
        site: String
    }
})

export const EnterpriseModel = mongoose.model<EnterpriseModelInterface>('Enterprise', enterpriseSchema)