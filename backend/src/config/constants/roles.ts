export enum ROLE {
    ADMIN       = 'ADMIN',
    ANTREPRENOR = 'ANTREPRENOR', 
    CLIENT      = 'CLIENT'
}