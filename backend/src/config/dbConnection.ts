import mongoose, {Connection} from 'mongoose'

// TODO : put password in env
const url = 'mongodb+srv://anto:anto@cluster-0.tfnd6.mongodb.net/antreprenori-db?retryWrites=true&w=majority'

export function connectToDatabase(): void {
    mongoose
        .connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true, 
            useCreateIndex: true
        })
        .catch((err: any) => console.log(err.reason));

    const db: Connection = mongoose.connection;

    db.on('error', console.error.bind(console, 'Connection error:'));
    db.once('open', () => {
        console.log('Connected to MongoDB');
    });
}