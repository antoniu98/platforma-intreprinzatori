import nodemailer from "nodemailer"

let transporder: any;

export function initMailer() {
    transporder = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
          user: "3f6b443d5f0b40",
          pass: "f0a1a90caf8387"
        }
      });
      console.log(`Nodemailer init done.`)
}

export const sendMail = (options: any) => {
    let response: String = ""

    transporder.sendMail(options, (err: any, info: any) => {
        if (err) {
            console.log(`\n[sendMail]: Error at sending mail : ${err}`)
            response = "error"
            return
        }
        
        console.log(`\n[sendMail]: Sent mail with info: ${JSON.stringify(info)}`)
        response = info
    })

    return response
}