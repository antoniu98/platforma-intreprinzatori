import express, { Application, NextFunction, Request, Response } from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import createError from 'http-errors';

import {connectToDatabase} from './config/dbConnection'
import { initMailer } from './config/mailConnection';
import { router } from './controllers/router';

const cors = require('cors')
const errorMiddleware = (err: any, req: Request, res: Response, next: NextFunction) => {
    if (err) {
      console.log(`err: ${err}`)
      res.status(err.httpStatusCode)
      res.send({err: err.message})
    } else {
      next(err)
    }
}

/* middleware */
const app: Application = express()
let whitelist = ['http://localhost:3000']
app.use(cors({
  origin: function(origin:any, callback:any){
    // allow requests with no origin 
    if(!origin) return callback(null, true);
    if(whitelist.indexOf(origin) === -1){
      var message = 'The CORS policy for this origin doesnt ' +
                'allow access from the particular origin.';
      return callback(new Error(message), false);
    }
    return callback(null, true);
  }
}));
app.use(helmet())
app.use(morgan(':remote-addr - :remote-user [:date[web]] ":method :url HTTP/:http-version" :status :res[content-length]'));
app.use(express.json())
app.use('/api', router)
app.use(errorMiddleware)

/* DB connect */
connectToDatabase()

/* Mail init */
initMailer()

const port = process.env.PORT || 8888;
app.listen(port, () => {
    console.log(`App is listening on ${port}`);
});