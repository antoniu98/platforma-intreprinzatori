import express, {Router, NextFunction, Request, Response, json } from 'express';
import {UserModel} from '../models/user'


export const userRouter:Router = express.Router();

userRouter.get('/:email', 
    async (req: Request, res: Response) => {
        let {email} = req.params
        UserModel.findOne({email: email}, (err: any, result: any) => {
            return res.json(result)
        })
    }
)

userRouter.get('/', 
    async (req: Request, res: Response) => {
        UserModel.find((err: any, result: any) => {
            return res.json(result)
        })
    }
)

userRouter.post('/', 
    async (req: Request, res: Response) => {
        const user = new UserModel({
            email: req.body.email,
            fullName: req.body.fullName,
            role: req.body.role,
            password: req.body.password,
            firme: req.body.firme
        })

        try {
            const newUser = await user.save()
            return res.json(newUser)
        }
        catch (err) {
            return res.send('erorr: ' + err.message)
        }
    }
)