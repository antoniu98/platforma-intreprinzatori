import express, {Router, NextFunction, Request, Response } from 'express';
import {EnterpriseModel} from '../models/enterprise'


export const enterpriseRouter:Router = express.Router();


enterpriseRouter.post('/', 
    async (req: Request, res: Response) => {
        const enterprise = new EnterpriseModel({
            name: "firma mea",
            owner: "Anto",
            details: {
                registrationDate: new Date(),
                site: "www.firmamea.ro"
            }
        })

        try {
            const newEnterprise = await enterprise.save()
            return res.send('new enterprise added')
        }
        catch (err) {
            return res.send('erorr: ' + err.message)
        }
    }
)