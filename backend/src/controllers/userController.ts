import express, {Router, NextFunction, Request, Response } from 'express';
import {UserModel} from '../models/user'
import {ServerError} from '../utils/serverError'
import { RoleAuthorizer } from "../middlewares/authorization/roleAuthorizer"

/* PRODUCT e acelasi lucru cu ADS -> trebuie redenumite */
export const userRouter:Router = express.Router();


class UserService {
    /* GET */
    public async getAllUsers(): Promise<any> {
        try {
            return await UserModel.find()
        } catch (err) {
            console.log(`\n[ServiceService.getAllUsers]: ${err}`)
            throw new ServerError('Eroare la gasirea tuturor userilor', 401)
        }
    }

    public async getUserById(id: String): Promise<any> {
        try {
            return await UserModel.find({_id : id})
        } catch (err) {
            console.log(`\n[ServiceService.getUserById]: ${err}`)
            throw new ServerError(`Eroare la gasirea userului ${id}`, 401)
        }
    }

    public async getUserByEmail(email: String): Promise<any> {
        try {
            return await UserModel.find({email : email})
        } catch (err) {
            console.log(`\n[ServiceService.getUserByEmail]: ${err}`)
            throw new ServerError(`Eroare la gasirea userului ${email}`, 401)
        }
    }

    /* CREATE, EDIT */  
    public async editUserById(id: String, data: any): Promise<any> {
        try {
            return await UserModel.updateOne(
            {
                _id : id
            }, data)
        } catch (err) {
            console.log(`\n[AdsService.editUserById]: ${err}`)
            throw new ServerError(`Eroare la editarea userului id ${id}`, 401)
        }
    }

    /* DELETE */
    public async deleteUserById(id: String): Promise<any> {
        try {
            return await UserModel.deleteOne({
                _id: id
            })
        } catch (err) {
            console.log(`\n[UserService.deleteUserById]: ${err}`)
            throw new ServerError(`Eroare la stergerea userului cu id:${id}`, 401)
        }
    }
}


class UserController {
    public static userService: UserService = new UserService()

    // GET
    public static async getAllUsers(req: Request, res: Response, next: NextFunction) {
        try {
            const result = await UserController.userService.getAllUsers()
            res.send({users: result})
        } catch (err) {
            next(err)
        }
    }

    public static async getOwnUser(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.body.user
            const result = await UserController.userService.getUserById(id)
            res.send({users: result})
        } catch (err) {
            next(err)
        }
    }

    // CREATE / EDIT
    public static async editOwnUser(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params
            const { email, fullName } = req.body
            const data = {
                email: email,
                fullName: fullName
            }
            const result = await UserController.userService.editUserById(id, data)
            res.send({users: result})
        } catch (err) {
            next(err)
        }
    }

    public static async editUserByID(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params
            const { email, fullName, role } = req.body
            const data = {
                ...(email            && {email: email}),
                ...(fullName         && {fullName: fullName}),
                ...(role             && {role: role}),
            }
            const result = await UserController.userService.editUserById(id, data)
            res.send({users: result})
        } catch (err) {
            next(err)
        }
    }

    // DELETE
    public static async deleteUserById(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params
            const result = await UserController.userService.deleteUserById(id)
            res.send({users: result})
        } catch (err) {
            next(err)
        }
    }
}

// GET
userRouter.get   ('/',         RoleAuthorizer.adminRole,  UserController.getAllUsers)
userRouter.get   ('/own',      RoleAuthorizer.anyRole,    UserController.getOwnUser)
// CREATE / EDIT
// userRouter.put   ('/own',      RoleAuthorizer.anyRole,    UserController.editOwnUser)
userRouter.put   ('/:id', RoleAuthorizer.adminRole,  UserController.editUserByID)
// DELETE
userRouter.delete('/:id',      RoleAuthorizer.adminRole,  UserController.deleteUserById)

// daca se modifica rolul trebuie dat login iar