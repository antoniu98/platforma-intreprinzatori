import express, {Router, NextFunction, Request, Response } from 'express'
import {UserModel} from '../models/user'
import {ServerError} from '../utils/serverError'

import {sendMail} from '../config/mailConnection'
import {hashPassword, comparePassword} from '../utils/passwordHasher'
import * as tokenService from '../utils/tokenService'


class AuthService {
    // REGISTER
    public async hashPassword(password: String): Promise<String> {
        try {
            return await hashPassword(password)
        } catch (err) {
            console.log(`\n[hashPassword]: eroare la hashingul parolei: ${err}`)
            throw new ServerError('Eroare. Reincercati inregistrarea', 401)
        }
    }

    public async checkUserInDB(email: String): Promise<Boolean> {
        let user
        try {
            user = await UserModel.findOne({ email: email})
        }
        catch (err) {
            console.log(`\n[addUserToDb]: Eroare la verificarea userului in BD: ${err}`)
        }

        if (user)
            if (user.confirmed)
                throw new ServerError('Aceasta adresa de mail este deja inregistrata.', 401)
            else {//e inregistrat, POATE a expirat linkul si vrea sa se inregistreze iar
                await UserModel.deleteOne({email: email})
                return false
            }
        else
            return false
    }

    public async addUserToDb(email: String, fullName: String, hashedPassword: String, role: String = "CLIENT"): Promise<void> {
        if (role != "CLIENT" && role != "ANTREPRENOR")
            throw new ServerError("Rolul declarat este invalid.", 401)
        const user = new UserModel({
            email: email,
            fullName: fullName,
            password: hashedPassword,
            role: role,
            confirmed: false
        })
        try {
            await user.save()
        }
        catch (err) {
            console.log(`\n[addUserToDb]: Eroare la adaugarea userului in BD: ${err}`)
            throw new ServerError('Eroare la adaugarea in baza de date', 401)
        }
    }

    public async createToken(email: String): Promise<String> {
        try {
            return await tokenService.createExpirationJwtToken({
                email: email
            })
        } catch(err) {
            throw new ServerError("Eroare la crearea tokenului!", 401);
        }
    }

    public async sendEmailTo(token: String, email: String): Promise<void> {
        console.log(`\n[sendEmailTo]: Start sendEmailTo().`)

        const url = `http://localhost:8888/api/auth/confirmation/${token}`
        const options = {
            from: "antoniu.neacsu@stud.acs.upb.ro",
            to: email,
            subject: "Confirmarea inregistrarii.",
            html: `Apasati <a href="${url}">aici</a> in maximum 15 minute pentru a confirma inregistrarea.`,
        }
        const info: String = sendMail(options)
        if (info == "error") {
            throw new ServerError("Eroare la trimiterea mailului de confirmare", 401)
        }
        else
            console.log(`\n[sendEmailTo]: Mail was sent and received with info: ${info}`)
    }

    public async undoUserSave(email:String): Promise<void> {
        console.log(`\n[undoUserSave]: Start undoUserSave().`)

        await UserModel.deleteOne({ email: email}, (err) => {
            if (err)
                console.log(`\n[undoUserSave]: Eroare la stergerea userului: ${err}`)
        })
    }


    // LOGIN
    public async findUser(email: String, password: String): Promise<any> {
        /* 1. Get the user from db*/
        const user = await UserModel.findOne({
            'email': email
        }, 
            (err:any, result:any) => {
                if (err) { throw new ServerError(`error: Nu exista un user cu aceasta adresa de mail: ${email}`, 401) }
            }
        )

        /* 2. Check password hash */
        if (!user)
            throw new ServerError(`Nu exista un user cu aceasta adresa de mail: ${email}`, 401)
        const correctPassword: Boolean = await comparePassword(password, user.password)
        if (!correctPassword)
            throw new ServerError(`Parola introdusa este gresita.`, 401)

        return user
    }

    public async checkPassword(password: String, hashedPassword: String): Promise<void> {
        try {
            const correct: Boolean = await comparePassword(password, hashedPassword)
            if (!correct)
                throw new ServerError("Parola nu este corecta", 401)
        } catch(err) {
            throw new ServerError("Eroare la verificarea parolei.", 401)
        }
    }

    public async generateJwtLoginToken(data: any): Promise<String> {
        try {
            return await tokenService.createJwtToken(data)
        } catch (err) {
            throw new ServerError("Eroare la generarea tokenului jwt.", 401)
        }
    }


    // CONFIRMATION
    public async decryptRegistrationToken(token: String): Promise<any> {
        try {
            return await tokenService.verifyAndDecodeExpirationJwtToken(token)
        } catch (err) {
            throw new ServerError("Tokenul este posibil sa fi expirat. Reperati inregistrarea.", 401)
        }
    }

    public async confirmRegistrationForEmail(email: String): Promise<void> {
        try {
            const user = await UserModel.findOne({email: email})
            if (!user)
                throw new ServerError("User to be confirmed not found.", 401)
            /* update 'confirmed' field to 'true */
            user.confirmed = true
            await UserModel.updateOne({email: email}, user)
        } catch (err) {
            console.log(`[confirmRegistrationForUser]: Confirming user reg in db failed: ${err}`)
        }
    }
}


class AuthController {
    public static authService: AuthService = new AuthService()

    public static async login(req: Request, res: Response, next: NextFunction) {
        // TODO: check and serialize parameters
        try {
            /* 1. get user by email from db */
            let {email, password} = req.body
            let user = await AuthController.authService.findUser(email, password)

            /* 2. check password: if passw is wrong it will throw an error */
            await AuthController.authService.checkPassword(password, user.password)

            /* 3. Check that the mail is confirmed */
            if (!user.confirmed) {
                throw new ServerError("Contul nu este confirmat", 401)
            }

            /* 4. generate jwt token */
            const token = await AuthController.authService.generateJwtLoginToken({
                id: user._id,
                email: user.email,
                fullName: user.fullName, 
                role: user.role
            })

            /* 5. return the jwt token */
            res.send({
                token: token, 
                confirmed: true,
                user: {
                    id: user._id,
                    email: user.email,
                    fullName: user.fullName,
                    role: user.role
                }
            })
        } catch (err) {
            next(err)
        }
    }

    public static async register(req: Request, res: Response, next: NextFunction) {
        // TODO: check and serialize parameters
        const successString: String = "Inregistrarea a avut loc cu succes.\n" +  
                                    "Accesati mail-ul pentru a confirma contul si a avea acces la platforma."
        
        try {
            const {email, fullName, password, role} = req.body
            /* 1. cripteaza parola */
            const hashedPassword: String = await AuthController.authService.hashPassword(password)

            /* 2. check user in db: it will throw an error if it does not exist*/
            await AuthController.authService.checkUserInDB(email)

            /* 3. create token */
            let token: String = await AuthController.authService.createToken(email)

            /* 4. Add user in db */
            await AuthController.authService.addUserToDb(email, fullName, hashedPassword, role)

            /* 5. send verification email */
            await AuthController.authService.sendEmailTo(token, email)

            /* 6. Send message */
            res.send({
                message: successString
            })
        } catch (err) {
            next(err)
        }
    }

    public static async confirmation(req: Request, res: Response, next: NextFunction) {
        const {token} = req.params
        
        try {
            /* 1. Decrypt token and get the user fields */
            const expirationToken = await AuthController.authService.decryptRegistrationToken(token)
            const email = expirationToken.email

            /* Update in db 'confirmation field' -> true */
            await AuthController.authService.confirmRegistrationForEmail(email)

            res.send({
                confirmed: true
            })
        } catch (err) {
            next(err)
        }
    }
}


export const authRouter:Router = express.Router();
authRouter.post('/login',               AuthController.login)
authRouter.post('/register',            AuthController.register)
authRouter.get('/confirmation/:token',  AuthController.confirmation)