import express, {Router, NextFunction, Request, Response } from 'express';
import {ProductModel as AdModel} from '../models/product'
import {ServerError} from '../utils/serverError'
import { RoleAuthorizer } from "../middlewares/authorization/roleAuthorizer"


/* PRODUCT e acelasi lucru cu ADS -> trebuie redenumite */
export const adsRouter:Router = express.Router();


class AdsService {
    /* GET */
    public async getAllAds(): Promise<any> {
        try {
            return await AdModel.find()
        } catch (err) {
            console.log(`\n[AdsService.getAllAds]: ${err}`)
            throw new ServerError('Eroare la gasirea tuturor anunturilor', 401)
        }
    }

    public async getAdById(id: String): Promise<any> {
        try {
            return await AdModel.findOne({
                _id: id
            })
        } catch (err) {
            console.log(`\n[AdsService.getAdById]: ${err}`)
            throw new ServerError(`Eroare la gasirea anuntului cu id:${id}`, 401)
        }
    }

    public async getAllAdsByFilter(enterpriseName: String, name: any): Promise<any> {
        try {
            return await AdModel.find({
                ...(enterpriseName  && {enterpriseName: enterpriseName}),
                ...(name            && {name: name}),
            })
        } catch (err) {
            console.log(`\n[AdsService.getAllAdsByFilter]: ${err}`)
            throw new ServerError('Eroare la filtrarea anunturilor', 401)
        }
    }

    /* CREATE, EDIT */
    public async createAd(enterpriseName: String, adName: String, description: String, email: String): Promise<any> {
        try {
            const adModel = new AdModel({
                enterpriseName: enterpriseName,
                name: adName,
                description: description,
                email: email
            })
            return await adModel.save()
        } catch (err) {
            console.log(`\n[AdsService.createAd]: ${err}`)
            throw new ServerError('Eroare la adaugare anunt', 401)
        }
    }

    public async updateAdById(id: String, enterpriseName: String, name:String, description: String, email: String): Promise<any> {
        try {
            return await AdModel.updateOne(
            {
                _id : id
            }, 
            {
                ...(enterpriseName  && {enterpriseName: enterpriseName}),
                ...(name            && {name: name}),
                ...(description     && {description: description}),
                ...(email           && {email: email})
            })
        } catch (err) {
            console.log(`\n[AdsService.updateAdById]: ${err}`)
            throw new ServerError(`Eroare la editarea anuntului id ${id}`, 401)
        }
    }

    /* DELETE */
    public async deleteAdById(id: String): Promise<any> {
        try {
            return await AdModel.deleteOne({
                _id: id
            })
        } catch (err) {
            console.log(`\n[AdsService.deleteAdById]: ${err}`)
            throw new ServerError(`Eroare la stergerea anuntului cu id:${id}`, 401)
        }
    }
}


class AdsController {
    public static adsService: AdsService = new AdsService()

    /* GET */
    public static async getAllAds(req: Request, res: Response, next: NextFunction) {
        try {
            const ads = await AdsController.adsService.getAllAds()
            res.send({ ads: ads})
        } catch (err) {
            next(err)
        }
    }

    public static async getAdById(req: Request, res: Response, next: NextFunction) {
        try {
            const {id} = req.params
            const ad = await AdsController.adsService.getAdById(id)
            res.send({ads: ad})
        } catch (err) {
            next(err)
        }
    }

    public static async getAdByFilter(req: Request, res: Response, next: NextFunction) {
        try {
            const {enterpriseName, name} = req.body
            const ads = await AdsController.adsService.getAllAdsByFilter(enterpriseName, name)
            res.send({ads: ads})
        } catch (err) {
            next(err)
        }
    }

    public static async getOwnAds(req: Request, res: Response, next: NextFunction) {
        try {
            const enterpriseName = req.body.user.name
            const ads = await AdsController.adsService.getAllAdsByFilter(enterpriseName, null)
            res.send({ads: ads})
        } catch (err) {
            next(err)
        }
    }

    /* CREATE, EDIT */
    public static async createAd(req: Request, res: Response, next: NextFunction) {
        try {
            const {enterpriseName, adName, description, email} = req.body
            const confirmation = await AdsController.adsService.createAd(enterpriseName, adName, description, email)
            res.send({ads: confirmation})
        } catch (err) {
            next(err)
        }
    }

    public static async editAdById(req: Request, res: Response, next: NextFunction) {
        try {
            const { id } = req.params
            const { enterpriseName, adName, description, email } = req.body
            const response = await AdsController.adsService.updateAdById(id, enterpriseName, adName, description, email)
            res.send({ads : response})
        } catch (err) {
            next(err)
        }
    }

    /* DELETE */
    public static async deleteAdById(req: Request, res: Response, next: NextFunction) {
        try {
            const {id} = req.params
            const result = await AdsController.adsService.deleteAdById(id)
            res.send({ads: result})
        } catch (err) {
            next(err)
        }
    }
}


/* GET */
adsRouter.get   ('/',           RoleAuthorizer.anyRole,         AdsController.getAllAds)
adsRouter.get   ('/find/:id',   RoleAuthorizer.anyRole,         AdsController.getAdById)
adsRouter.get   ('/filter',     RoleAuthorizer.anyRole,         AdsController.getAdByFilter)
adsRouter.get   ('/my-ads',     RoleAuthorizer.enterpriserRole, AdsController.getOwnAds)

/* CREATE, EDIT */
adsRouter.post  ('/',           RoleAuthorizer.enterpriserRole, AdsController.createAd)
adsRouter.put   ('/:id',        RoleAuthorizer.enterpriserRole, AdsController.editAdById)

/* DELETE */
adsRouter.delete('/:id',        RoleAuthorizer.enterpriserRole, AdsController.deleteAdById)