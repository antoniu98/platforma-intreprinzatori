import express from 'express';

import {userRouter}         from './userController'
import {enterpriseRouter}   from './enterpriseController'
import {adsRouter}          from './adsController'
import {authRouter}         from './authController'
import {authorizeTokenAndExtractUser} from "../middlewares/authorization/jwtAuthorizer"

export const router = express.Router();

router.use('/users',  authorizeTokenAndExtractUser, userRouter)
router.use('/firma',  authorizeTokenAndExtractUser, enterpriseRouter)
router.use('/ads',    authorizeTokenAndExtractUser, adsRouter)
router.use('/auth',   authRouter)