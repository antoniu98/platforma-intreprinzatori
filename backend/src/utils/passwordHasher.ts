import bcryptjs from 'bcryptjs'


export async function hashPassword(plainTextPassword: any): Promise<any>{
    const salt = await bcryptjs.genSalt()
    return await bcryptjs.hash(plainTextPassword, salt)
}

export async function comparePassword(plainTextPassword: any, hashedPassword: any): Promise<Boolean>{
    return await bcryptjs.compare(plainTextPassword, hashedPassword)
}