import {ServerError} from './serverError'

const jwt = require('jsonwebtoken')
const secret = "change_this_secret"
const options = {
    issuer: "Anto", //process.env.JWT_ISSUER,
    subject: "Verification token", //process.env.JWT_SUBJECT,
}
const optionsExpirationToken = {...options, expiresIn: '15m'}

/* Session token */
export async function createJwtToken(data: any): Promise<String> { 
    return await jwt.sign(data, secret, options)
}

export async function verifyAndDecodeJwtToken(token: String): Promise<String> {
    try {
        return await jwt.verify(token, secret, options)
    } catch (err) {
        console.trace(err);
        throw new ServerError(`Eroare la decriptarea tokenului! ${err}`, 401);
    }
}

/* Expiration token for registration */
export async function createExpirationJwtToken(data: any): Promise<String> { 
    return await jwt.sign(data, secret, optionsExpirationToken)
}

export async function verifyAndDecodeExpirationJwtToken(token: String): Promise<String> { 
    try {
        return await jwt.verify(token, secret, optionsExpirationToken)
    } catch (err) {
        console.trace(err);
        throw new ServerError(`Repetati inregistrarea. Eroare la decriptarea tokenului! ${err}`, 401);
    }
}